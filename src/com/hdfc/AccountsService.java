package com.hdfc;

public interface AccountsService
{
	 //declaration ,essential details
     public void addAccount(Account acc);
     public void getAllAccount();
     public void getAccountByAccNo(String accNo);
     
}
