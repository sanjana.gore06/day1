package com.hdfc;
import java.time.LocalDate;
public class Demo2 
{
  public static void main(String[] args)
  {
	  System.out.println("welcome ");
	  AccountsServiceImpl accService = new AccountsServiceImpl(); 
	  Account acc= null;
	  acc = new SavingAccount("SA-dcc","dcxcd",5.6f,2000, null);
	  accService.addAccount(acc);
	  acc = new Creditcard("CC-dcndj","dcd",24.5f,"121367",LocalDate.of(2022,12,10),LocalDate.of(2022, 12, 01));
	  accService.addAccount(acc);
	  acc= new Loan("cndxhcj","dcxc",8.9f,"2323",30000,10);
	  accService.addAccount(acc);
	  accService.getAllAccount();
	  
	  
	  System.out.println("-----------------------------");
	  
	  
	  accService.getAccountByAccNo("SA-dcc");
  }
}
