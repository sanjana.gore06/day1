package com.hdfc;

public class Loan extends Account 
{
    private String loanNumber;
    private double loanamount;
    private int loanDuration;
    private static float roi;
    
    
	public Loan() {
		// TODO Auto-generated constructor stub
	}
 
	public Loan(String accNo, String name, float roi, String loanNumber, double loanamount, int loanDuration) {
		super(accNo, name, roi);
		this.loanNumber = loanNumber;
		this.loanamount = loanamount;
		this.loanDuration = loanDuration;
	}

	public String getLoanNumber() {
		return loanNumber;
	}
	public void setLoanNumber(String loanNumber) {
		this.loanNumber = loanNumber;
	}
	public double getLoanamount() {
		return loanamount;
	}
	public void setLoanamount(double loanamount) {
		this.loanamount = loanamount;
	}
	public int getLoanDuration() {
		return loanDuration;
	}
	public void setLoanDuration(int loanDuration) {
		this.loanDuration = loanDuration;
	}
	@Override
	public String toString()
	{
		return "Loan [loanNumber=" + loanNumber + ", loanamount=" + loanamount + ", loanDuration=" + loanDuration
				+ ", getAccNo()=" + getAccNo() + ", getName()=" + getName() + ", getRoi()=" + getRoi() + "]";
	}
	
	public static float getRoi() {
		return roi;
	}

	public static void setRoi(float roi) {
		Loan.roi = roi;
	}

	public void getInterest()
	{
		double interest= (loanamount*loanDuration)/100;
		System.out.println(interest);
	}
	

}
