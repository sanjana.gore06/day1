package com.hdfc;

public class SavingAccount extends Account{

		private double balance;
		private String typeOfSavingAccount;
		private static float roi;
		
	 

		
		public SavingAccount(String accNo, String name, float roi, double balance, String typeOfSavingAccount) {
			super(accNo, name, roi);
			this.balance = balance;
			this.typeOfSavingAccount = typeOfSavingAccount;
		}



		@Override
		public String toString() {
			return "SavingAccount [balance=" + balance + ", typeOfSavingAccount=" + typeOfSavingAccount
					+ ", getAccNo()=" + getAccNo() + ", getName()=" + getName() + ", getRoi()=" + getRoi() + "]";
		}


		public static float getRoi() {
			return roi;
		}



		public static void setRoi(float roi) {
			SavingAccount.roi = roi;
		}



		public double getBalance() {
			return balance;
		}

		public void setBalance(double balance) {
			this.balance = balance;
		}

		public String getTypeOfSavingAccount() {
			return typeOfSavingAccount;
		}

		public void setTypeOfSavingAccount(String typeOfSavingAccount) {
			this.typeOfSavingAccount = typeOfSavingAccount;
		}
		
		public void getInterest()
		{
			double interest = balance*getRoi()/100;
			System.out.println("Interest is : "+interest);
		}
		
		
	}


